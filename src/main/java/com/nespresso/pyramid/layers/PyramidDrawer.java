package com.nespresso.pyramid.layers;

import com.nespresso.pyramid.layers.layer.Layer;
import com.nespresso.pyramid.layers.layer.LayerDrawer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidDrawer implements Drawer, LayersVisitor {

    private static final String UNDERSCORE_BLOCKS = "_";
    private static final String EMPTY_BLOCKS = " ";
    private static final String NEW_LINE = "\n";
    private final List<String> layers;
    private final LayerDrawer layerDrawer;

    public PyramidDrawer(LayerDrawer layerDrawer) {
        layers = new ArrayList<String>();
        this.layerDrawer = layerDrawer;
    }

    public String draw() {
        equalize();
        StringBuilder sb = new StringBuilder();
        if (layers.size() > 0) {
            for (int i = layers.size() - 1; i > 0; i--) {
                sb.append(layers.get(i));
                sb.append(NEW_LINE);
            }
            sb.append(layers.get(0));
        }
        return sb.toString();
    }

    private String equalizeWith(String layer, String secondLayer, String block) {
        int diff = (secondLayer.length() - layer.length()) / 2;
        StringBuilder additional = new StringBuilder();

        for (int i = 0; i < diff; i++) {
            additional.append(block);
        }

        StringBuilder sb = new StringBuilder();
        sb.append(additional)
                .append(layer)
                .append(additional);
        return sb.toString();
    }

    public void visit(Layer layer) {
        layer.accept(layerDrawer);
        layers.add(layerDrawer.draw());
    }

    private void equalize() {
        List<String> equalized = new ArrayList<String>();

        if (layers.size() > 0) {
            for (int i = layers.size() - 1; i > 0; i--) {
                String equalizeWithPrevious = equalizeWith(layers.get(i), layers.get(i - 1), UNDERSCORE_BLOCKS);
                equalized.add(equalizeWith(equalizeWithPrevious, layers.get(0), EMPTY_BLOCKS));
            }
            equalized.add(layers.get(0));

            Collections.reverse(equalized);
            this.layers.clear();
            this.layers.addAll(equalized);
        }

    }
}
