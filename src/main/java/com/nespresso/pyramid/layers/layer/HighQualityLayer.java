package com.nespresso.pyramid.layers.layer;

public class HighQualityLayer extends Layer {
    public HighQualityLayer(int size) {
        super(size);
        this.blockRepresentation = "X";
    }

    @Override
    public boolean willCollapse(Layer layer) {
        if (size > layer.size) {
            return true;
        }
        return false;
    }
}
