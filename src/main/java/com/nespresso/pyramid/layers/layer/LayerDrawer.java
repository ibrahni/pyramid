package com.nespresso.pyramid.layers.layer;

import com.nespresso.pyramid.layers.Drawer;

public class LayerDrawer implements Drawer,LayerVisitor {

    private int size;
    private String presentation;

    public void visit(int size, String blockRepresentation) {
        refresh();
        this.size = size;
        this.presentation = blockRepresentation;
    }

    private void refresh() {
        size = 0;
        presentation = "";
    }

    public String draw() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(presentation);
        }
        return sb.toString();
    }
}
