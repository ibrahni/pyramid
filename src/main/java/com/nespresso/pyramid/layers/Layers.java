package com.nespresso.pyramid.layers;

import com.nespresso.pyramid.layers.layer.Layer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Layers {
    private List<Layer> layers;

    public Layers() {
        layers = new ArrayList<Layer>();
    }

    public void add(Layer layer) {
        layers.add(layer);
        refresh();
    }

    private void refresh() {
        int size = layers.size();
        if (size >= 2) {
            if (layers.get(size - 1).willCollapse(layers.get(size - 2))) {
                layers.remove(size - 2);
            }
        }
    }

    public void accept(LayersVisitor visitor) {
        for (Iterator it = layers.iterator(); it.hasNext(); ) {
            visitor.visit((Layer) it.next());
        }
    }
}
